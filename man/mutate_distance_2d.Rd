% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/distance-measures.R
\name{mutate_distance_2d}
\alias{mutate_distance_2d}
\title{Calculate distance between current position and 2D peak.}
\usage{
mutate_distance_2d(frame)
}
\description{
Calculate distance between current position and 2D peak.
}
