% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/landscape-plots.R
\name{make_landscape}
\alias{make_landscape}
\title{Make a landscape wireframe plot.}
\usage{
make_landscape(Landscape, title = "")
}
\description{
Make a landscape wireframe plot.
}
