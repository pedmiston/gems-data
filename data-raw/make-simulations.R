library(tidyverse)
source("R/parse-position-strings.R")

Bots <- list.files("data-raw/simulations", "*.csv", full.names = TRUE) %>%
  map(read_csv) %>%
  bind_rows() %>%
  parse_pos("pos", "current_") %>%
  parse_pos("selected", "selected_") %>%
  arrange(subj_id, trial)

devtools::use_data(Bots, overwrite = TRUE)
