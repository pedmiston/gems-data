#' Make a landscape wireframe plot.
#' @export
make_landscape <- function(Landscape, title = "") {
  Landscape %>%
    filter(x %% 5 == 0, y %% 5 == 0) %>%
    lattice::wireframe(
      score ~ x * y, data = .,
      xlab = "orientation", ylab = "bar width",
      main = grid::textGrob(title)
    )
}
