#' Data for SimpleHill landscape.
#'
#' @section Description:
#'
#' \itemize{
#'   \item x Grid position. Int between 0--99
#'   \item y Grid position. Int between 0--99
#'   \item ori Gem orientation.
#'   \item sf Gem spatial frequency.
#'   \item score Height of landscape at grid position.
#' }
"SimpleHill"
