#' Label team strategy based on instructions and ancestor instructions.
#' @export
label_team_strategy <- function(frame) {
  teams <- expand.grid(
    instructions = c("orientation", "spatial_frequency"),
    ancestor_instructions = c("orientation", "spatial_frequency", NA),
    stringsAsFactors = FALSE
  ) %>%
    as_data_frame()
  teams$team_strategy <- c("congruent", "complementary", "complementary", "congruent", "isolated", "isolated")
  if(missing(frame)) return(teams)
  left_join(frame, teams)
}

#' Recode team strategy for plots and models.
#'
#' If no data is given, returns the map.
#'
#' @export
recode_team_strategy <- function(frame) {
  strategies <- c("isolated", "congruent", "complementary")
  labels <- strategies
  strategy_map <- data_frame(
    team_strategy = strategies,
    team_strategy_label = factor(strategies, levels = strategies, labels = labels)
  )

  treatment_contrasts <- contr.treatment(strategies, base = 3) %>%
    as.data.frame() %>%
    rename(
      isolated_v_complementary = isolated,
      congruent_v_complementary = congruent
    ) %>%
    rownames_to_column("team_strategy")

  helmert_contrasts <- contr.helmert(strategies) %>%
    as.data.frame() %>%
    rename(
      helmert_isolated_v_congruent = V1,
      helmert_complementary_v_all = V2
    ) %>%
    rownames_to_column("team_strategy")

  strategy_map <- strategy_map %>%
    left_join(treatment_contrasts) %>%
    left_join(helmert_contrasts)

  if(missing(frame)) return(strategy_map)
  left_join(frame, strategy_map)
}

#' Replace the starting position for first gen participants after 20 trials.
#' @export
replace_first_gen_start_pos_after_20 <- function(gems_data) {
  first_gen_start_pos <- gems_data %>%
    filter(generation == 1, trial == 20, landscape_ix != 0) %>%
    select(subj_id, landscape_ix, starting_pos = pos)

  first_gen_after_20 <- with(gems_data, (generation == 1) & (trial >= 20) & (landscape_ix != 0))
  gems_data <- bind_rows(
    gems_data %>%
      filter(!first_gen_after_20),
    gems_data %>%
      filter(first_gen_after_20) %>%
      select(-starting_pos) %>%
      left_join(first_gen_start_pos)
  )
  gems_data %>%
    arrange(subj_id, landscape_ix, trial)
}
