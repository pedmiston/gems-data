simple <- data_frame(
  subj_id = 1,
  trial = 1,
  current_score = 50,
  gem_x = 1:6,
  gem_y = 1:6,
  gem_score = 1:6
)

tie_scores <- data_frame(
  subj_id = 1,
  trial = 1,
  current_score = 50,
  gem_x = c(1, 2, 3, 4, 5, 5),
  gem_y = c(1, 2, 3, 4, 5, 5),
  gem_score = c(1, 2, 3, 4, 5, 5)
)

out_of_order <- data_frame(
  subj_id = 1,
  trial = 1,
  current_score = 50,
  gem_x = 6:1,
  gem_y = 6:1,
  gem_score = 6:1
)

straddle_peak <- data_frame(
  subj_id = 1,
  trial = 1,
  current_score = 50,
  gem_x = c(48, 49, 50, 51, 52, 53),
  gem_y = c(47, 48, 49, 50, 51, 52),
  gem_score = c(48, 49, 51, 49, 48, 47)
)

context("stim ranks")

test_that("stims are ranked by score", {
  result <- rank_stims_in_trial(simple)
  expect_equal(result$gem_score_rank, 6:1)
})

test_that("stims are ranked when out of order", {
  result <- rank_stims_in_trial(out_of_order)
  expect_equal(result$gem_score_rank, 1:6)
})

test_that("tie scores use min rank", {
  result <- rank_stims_in_trial(tie_scores)
  expect_equal(result$gem_score_rank, c(6, 5, 4, 3, 1, 1))
})

context("relative positions")

result <- rank_stims_in_trial(simple)

test_that("gem positions are ranked", {
  expect_equal(result$gem_x_rank, 6:1)
  expect_equal(result$gem_y_rank, 6:1)
})

test_that("gem absolute relative positions are calculated correctly", {
  expect_equal(result$gem_x_rel, 50 - 1:6)
  expect_equal(result$gem_y_rel, 50 - 1:6)
})

test_that("gem rel positions are normalized", {
  expect_equal(result$gem_x_rel_c, seq(-2.5, 2.5, length.out = 6))
  expect_equal(result$gem_y_rel_c, seq(-2.5, 2.5, length.out = 6))
})

context("straddle peak")

result <- rank_stims_in_trial(straddle_peak)

test_that("gem scores are relative to current score", {
  expect_equal(result$gem_rel_score, c(-2, -1, 1, -1, -2, -3))
})

test_that("gem positions are ranked relative to peak", {
  expect_equal(result$gem_x_rank, c(4, 2, 1, 2, 4, 6))
  expect_equal(result$gem_y_rank, c(6, 4, 2, 1, 2, 4))
})

test_that("gem positions are relative to peak", {
  expect_equal(result$gem_x_rel, c(2, 1, 0, 1, 2, 3))
  expect_equal(result$gem_y_rel, c(3, 2, 1, 0, 1, 2))
})
