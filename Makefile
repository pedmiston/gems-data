.PHONY: data install docs
all: data install
data:
	Rscript data-raw/make-all.R
install: docs
	Rscript -e "devtools::install()"
docs:
	Rscript -e "devtools::document()"
