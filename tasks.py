from pathlib import Path
from invoke import task


R_PKG = Path(__file__).absolute().parent


@task
def install(ctx, verbose=True, make_data_too=False):
    """Install the gems R package."""
    if make_data_too:
        make(ctx, verbose=verbose)

    cmd = 'cd {R_pkg} && Rscript -e {R_cmds!r}'
    R_cmds = """\
    library(devtools)
    document()
    install()
    """.split()
    ctx.run(cmd.format(R_pkg=R_PKG, R_cmds=';'.join(R_cmds)),
            echo=verbose)

@task
def make(ctx, verbose=True):
    """Compile the gems data to .rda files."""
    cmd = 'cd {R_pkg} && Rscript data-raw/make-all.R'
    ctx.run(cmd.format(R_pkg=R_PKG), echo=verbose)
